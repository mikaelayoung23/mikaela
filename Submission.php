<?php

class Submission extends DataObject
{
    private static $db = [
        'FirstName' => 'varchar(255)',
        'LastName' => 'varchar(255)',
        'Email' => 'varchar(255)',
        'Phone' => 'varchar(255)',
        'Field1' => 'varchar(255)',
        'Field2' => 'varchar(255)',
        'TnC' => 'boolean',
        'OptIn' => 'boolean',
        'Field3' => 'boolean',
    ];

    private static $has_one = [
        'Page' => 'SubmissionPage',
    ];

    private static $summary_fields = [
        'FirstName',
        'LastName',
        'Email',
        'Phone',
        'Field1',
        'Field2',
        'TnC',
        'OptIn',
        'Field3',
    ];

    private static $field_labels = [
        'TnC' => 'T&C',
        'OptIn' => 'Opt-in',
    ];

    public function getExcelExportFields()
    {
        return [
            'Created' => 'datetime',
            'FirstName' => 'varchar(255)',
            'LastName' => 'varchar(255)',
            'Email' => 'varchar(255)',
            'Phone' => 'varchar(255)',
            'Field1' => 'varchar(255)',
            'Field2' => 'varchar(255)',
            'TnC' => 'varchar(255)',
            'OptIn' => 'varchar(255)',
            'Field3' => 'varchar(255)',
        ];
    }
}
