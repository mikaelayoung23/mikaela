<?php

class SubmissionPage extends Page
{

    // Note: Classes have been renamed and I have shortened the code a bit.

    // This is a page type for a form with that submits user input to the database,
    // and optionally sends an automated email notifying the admin (or whoever you
    // set it to) whenever a user submits their information.

    /**
     * @var array
     */
    private static $db = [
        // copy
        'LandingCopy' => 'HTMLText',
        'SubmissionCopy' => 'HTMLText',
        'ConfirmationCopy' => 'HTMLText',

        // Field settings
        'EnterIsEnabled' => "ENUM('Yes,No','Yes')",

        'Field1Active' => "ENUM('Yes,No','No')",
        'Field1Optional' => "ENUM('Yes,No','No')",
        'Field1Label' => 'Varchar(255)',

        'Field2Active' => "ENUM('Yes,No','No')",
        'Field2Optional' => "ENUM('Yes,No','No')",
        'Field2Label' => 'Varchar(255)',

        'Field3Active' => "ENUM('Yes,No','No')",
        'Field3Optional' => "ENUM('Yes,No','No')",
        'Field3Label' => 'Varchar(255)',

        // Email Auto Response Settings
        'EmailIsEnabled' => "ENUM('Yes,No','No')",
        'EmailFrom' => 'Varchar(255)',
        'EmailTo' => 'Varchar(255)',
    ];

    /**
     * @var array
     */
    private static $has_many = [
        'Submissions' => 'Submission',
    ];

    public function requireDefaultRecords()
    {
        parent::requireDefaultRecords();

        // check if we should update the name and title of the project
        $siteConfig = SiteConfig::current_site_config();
        if ($siteConfig->Title == 'Your Site Name') {
            $siteConfig->Title = 'Submission Page';
            $siteConfig->Tagline = '';
            $siteConfig->Theme = 'Submission';

            DB::alteration_message('SiteConfig adjusted.', 'created');
        }
    }


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Copy',
            [
                HTMLEditorField::create('LandingCopy', 'Landing Copy')
                    ->setRows(5)
                    ->setDescription('This copy will be displayed on the first page.'),
                SuccessMessage::create('The following will be displayed on the submission page.'),
                HTMLEditorField::create('SubmissionCopy', 'Submission Copy')
                    ->setRows(5)
                    ->setDescription('This copy will be displayed above the form.'),
                SuccessMessage::create('The following will be displayed on the confirmation page.'),
                HTMLEditorField::create('ConfirmationCopy', 'Confirmation Copy')
                    ->setRows(5)
                    ->setDescription('This copy will be after the submission of the form.'),
            ]
        );

        $fields->addFieldsToTab(
            'Root.Form Settings',
            [
                SuccessMessage::create('This is an optional text field and will be saved as Field1.'),
                OptionsetField::create('Field1Active', 'Is field one active?')
                    ->setSource($this->dbObject('Field1Active')->enumValues()),
                OptionsetField::create('Field1Optional', 'Is field one optional?')
                    ->setSource($this->dbObject('Field1Optional')->enumValues()),
                TextField::create('Field1Label', 'Field One Label'),

                SuccessMessage::create('This is an optional text field and will be saved as Field2.'),
                OptionsetField::create('Field2Active', 'Is field two active?')
                    ->setSource($this->dbObject('Field2Active')->enumValues()),
                OptionsetField::create('Field2Optional', 'Is field two optional?')
                    ->setSource($this->dbObject('Field2Optional')->enumValues()),
                TextField::create('Field2Label', 'Field Two Label'),

                SuccessMessage::create('This is an optional checkbox field and will be saved as Field3.'),
                OptionsetField::create('Field3Active', 'Is field three active?')
                    ->setSource($this->dbObject('Field3Active')->enumValues()),
                OptionsetField::create('Field3Optional', 'Is field three optional?')
                    ->setSource($this->dbObject('Field3Optional')->enumValues()),
                TextField::create('Field3Label', 'Checkbox Three Label'),
            ]
        );

        $fields->addFieldsToTab(
            'Root.Submissions',
            [
                OptionsetField::create('EnterIsEnabled', 'Submissions are enabled')
                    ->setSource($this->dbObject('EnterIsEnabled')->enumValues()),
                SuccessMessage::create(
                    'The list contains all accepted submissions. Unsuccessful submissions aren\'t included.'
                ),
                GridField::create(
                    'Submissions',
                    'Submissions',
                    $this->Submissions(),
                    GridFieldConfig_Base::create()
                        ->addComponent(new GridFieldDeleteAction())
                        ->addComponent(new GridFieldExcelExportButton())
                ),
                SuccessMessage::create(
                    'Auto response email on submission settings'
                ),
                OptionsetField::create('EmailIsEnabled', 'Email on submission is enabled')
                    ->setSource($this->dbObject('EmailIsEnabled')->enumValues()),
                TextField::create('EmailFrom', 'Email From Recipient')
                    ->setDescription('The address the email comes from'),
                TextField::create('EmailTo', 'Email To Recipient')
                    ->setDescription('The address the email is sent to'),
            ]
        );

        // add a explaination on the main tab.
        $fields->addFieldsToTab(
            'Root.Main',
            [
                SuccessMessage::create(
                    'The visual appearance, copy and functionality can be adjusted with in the tabs above.'
                ),
            ],
            'Title'
        );

        $fields->removeByName('Content');

        return $fields;
    }
}

//How to explain this?

class SubmissionPage_Controller extends Page_Controller
{
    /**
     * @var array
     */
    private static $allowed_actions = [
        'Landing',
        'Submission',
        'Form',
        'Confirmation',
    ];

    public function Form()
    {
        $requiredFields = ['FirstName', 'LastName', 'Email', 'Phone', 'TnC', 'OptIn'];

        $fields = FieldList::create(
            TextField::create('FirstName', 'First name'),
            TextField::create('LastName', 'Last name'),
            EmailField::create('Email', 'Email'),
            TextField::create('Phone', 'Phone'),
            CheckboxField::create('TnC', 'I agree to the <a href="#" data-featherlight="#terms">terms and conditions</a>.'),
            CheckboxField::create('OptIn', 'I agree to the <a href="#" data-featherlight="#privacy">privacy policy</a>.')
        );

        // Check if the field is set as active, and if so then add the field after whichever field we want
        // (rather than after all the fields). If it is not marked as optional then add it to the required fields array.
        if ($this->Field2Active == 'Yes') {
            $fields->insertAfter(TextField::create('Field2', $this->Field2Label), 'Phone');

            if ($this->Field2Optional == 'No') $requiredFields[] = 'Field2';
        }

        if ($this->Field1Active == 'Yes') {
            $fields->insertAfter(TextField::create('Field1', $this->Field1Label), 'Phone');

            if ($this->Field1Optional == 'No') $requiredFields[] = 'Field1';
        }

        if ($this->Field3Active == 'Yes') {
            $fields->insertAfter(CheckboxField::create('Field3', $this->Field3Label), 'OptIn');

            if ($this->Field3Optional == 'No') $requiredFields[] = 'Field3';
        }

        // return the form and create the form with the aforementioned details, and adding the submit action
        return Form::create(
            $this,
            'Form',
            $fields,
            FieldList::create(FormAction::create('FormSubmission', 'Submit')),
            RequiredFields::create($requiredFields)
        );
    }

    /**
     * @param array $data
     * @param Form $form
     */
    public function FormSubmission($data, $form)
    {
        // connect the submission with the actual page
        $data['PageID'] = $this->ID;

        // save it in our db
        Submission::create($data)->write();

        // adding an automated email whenever someone submits the form with their data
        // Uses a custom email template file and adds the extra fields too
        if ($this->EmailIsEnabled == 'Yes') {
            $email = new Email($this->EmailFrom, $this->EmailTo, 'New Submission');
            $email->setTemplate('Email');
            $email->populateTemplate(array_merge($data, [
                'Field1Active' =>  $this->Field1Active,
                'Field2Active' =>  $this->Field2Active,
                'Field3Active' =>  $this->Field3Active,
            ]));
            $email->send();
        }

        // redirect to the confirmation page
        return $this->redirect($this->Link('confirmation'));
    }

    public function Index()
    {
        return $this->redirect($this->Link('Landing'));
    }

    public function Landing()
    {
        return $this->customise($this->toMap())->renderWith(['SubmissionPage_Landing', 'Page']);
    }

    public function Submission()
    {
        return $this->customise($this->toMap())->renderWith(['SubmissionPage_Submission', 'Page']);
    }

    public function Confirmation()
    {
        return $this->customise($this->toMap())->renderWith(['SubmissionPage_Confirmation', 'Page']);
    }
}
