<?php

class SiteConfigExtension extends DataExtension
{
    /**
     * @var array
     */
    private static $db = [
        //Brand
        'BackgroundColor' => 'Varchar(7)',
        'InnerBgColor' => 'Varchar(7)',
        'ButtonColor' => 'Varchar(7)',
        'BaseFontColor' => 'Varchar(7)',
        'LinkHoverColor' => 'Varchar(7)',
        'FontFamily' => 'Varchar(255)',
        'FontWeight' => 'Varchar(3)',

        // Footer Links
        'TnCText' => 'HTMLText',
        'PrivacyText' => 'HTMLText',
        'WebsiteUrl' => 'Varchar(255)',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'BackgroundImage' => 'Image',
        'LogoImage' => 'Image',
        'FooterImage' => 'Image',
    ];

    /**
     * @var array
     */
    private static $has_many = [
        'Icons' => 'SocialMediaIcon',
    ];

    /**
     * @var array
     */
    private static $defaults = [
        'BackgroundColor' => '#ffffff',
        'InnerBgColor' => '#dddddd',
        'BaseFontColor' => '#363636',
        'LinkHoverColor' => '#ee2123',
        'FontFamily' => 'Titillium Web',
        'FontWeight' => '600',
        'WebsiteUrl' => 'www.google.co.nz',

        'TnCText' => '<h2>TERMS AND CONDITIONS</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
        'PrivacyText' => '<h2>PRIVACY POLICY</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.Brand',
            [
                SuccessMessage::create('The background image and color are optional values and can be used combined.'),
                ColorField::create('BackgroundColor', 'Background Color'),
                ColorField::create('InnerBgColor', 'Inner Background Color'),
                ColorField::create('ButtonColor', 'Button Color'),
                ColorField::create('BaseFontColor', 'Base Font Color'),
                ColorField::create('LinkHoverColor', 'Link Hover Color'),
                TextField::create('FontFamily', 'Font Family')
                    ->setDescription('Accepts any browser built in fonts.'),
                TextField::create('FontWeight', 'Font Weight')
                    ->setDescription('Accepts any browser built in font weight (increments of 100 up to 600).'),
                UploadField::create('BackgroundImage', 'Background Image'),
                UploadField::create('LogoImage', 'Logo Image'),
            ]
        );

        $fields->addFieldsToTab(
            'Root.Footer',
            [
                UploadField::create('FooterImage', 'Footer Image'),
                TextField::create('WebsiteUrl', 'Website Link')
                    ->setDescription('This is the link to the client website eg. www.google.co.nz'),
                GridField::create(
                    'Icons',
                    'Social Media Icons',
                    $this->owner->Icons(),
                    GridFieldConfig_RecordEditor::create()
                ),
                HTMLEditorField::create('TnCText', 'Terms and Conditions')
                    ->setRows(6),
                HTMLEditorField::create('PrivacyText', 'Privacy Policy')
                    ->setRows(6),
            ]
        );
    }

}
