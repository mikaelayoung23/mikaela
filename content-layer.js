var contentLayer = function(e){

  // Ensure the initial state of the navigation is done
  jQuery.each(
    $('.content-layer .content-layer-label'),
    function (index, element) {
      // add the active class to the first nav label
     $(element).children(':first').children(':first').addClass($activeClass);
    }
  );

  // Initial state of the first layer
  jQuery.each(
    $('.content-layer-box'),
    function (index, element) {
      // add the active class to the first items content
     $(element).children(':first').addClass($activeClass);
    }
  );

  // on click we should swap the 'active' class out.
  $('.content-layer-inline h2').click(function(e){

    // find out which label has been clicked and display the related content
    id = $(this).html().replace(/ /g, '-');

    // find the section we are using atm
    section = $(this).parents('.section').attr('ID');

    if(width > 640) {

      // adjust which heading and section are active.
      $('#' + section + ' .content-layer-inline').children().removeClass('active');
      $('#' + section + ' .content-layer-item').removeClass('active');
      $('#nav-' + id).addClass('active');
      $('#' + id).addClass('active');

    } else {
      // Fade the content items in and out instead of adding and removing classes so the content
      // gets pushed down on mobile
      $('#' + section + ' .content-layer-inline').children().removeClass('active');
      $('#' + section + ' .content-layer-item').fadeOut("slow");
      $('#nav-' + id).addClass('active');
      $('#' + id).fadeIn("slow");
    }
  });
}();
